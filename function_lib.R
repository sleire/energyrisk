library(timeSeries)
library(stargazer)

# spot price decsriptive statistics
calc_summary <- function(ret){
  dat <- rbind(apply(ret, 2, length), 
               apply(ret, 2, function(x) {mean(x, na.rm=TRUE)}), 
               apply(ret, 2, function(x){sd(x, na.rm=TRUE)}), 
               apply(ret, 2, function(x){var(x, na.rm=TRUE)}))
  Skewness <- timeSeries::colSkewness(ret)
  Kurtosis <- timeSeries::colKurtosis(ret)
  #Jarque.Bera <- sapply(1:dim(ret)[2], function(i) tseries::jarque.bera.test(ret[,i])$statistic)
  
  dat <- rbind(dat, Skewness, Kurtosis, 
               #Jarque.Bera, 
               apply(ret, 2, function(x) {quantile(x, na.rm=TRUE)}))
  rownames(dat) <- c("Observations", "Mean", "Std.dev.", "Variance",
                     "Skewness", "Kurtosis", 
                     #"Jarque-Bera",
                     "Min", "1 Quartile", "Median", "3 Quartile", "Max")
  round(dat, 4)
}


# hedging strategy evaluation
eval_strat <- function(strat_obj){
  
  # difference between portfolio price and market price at contract expiry %
  expiry_diff <- summary(strat_obj)$Stats[4, 7]/summary(strat_obj)$Stats[4, 1]
    
  # max diff portfolio price vs market price
  max_diff <- max(strat_obj@Results$Portfolio/strat_obj@Results$Market)
  
  
  # target price protection
  target_diff <- summary(strat_obj)$Stats[4, 7]/summary(strat_obj)$Stats[1, 6]

  
  # max diff portfolio price vs market price
  max_target_diff <- max(strat_obj@Results$Portfolio/strat_obj@Results$Target)
  
  # rebalancing and churn rate
  churn <- round(summary(strat_obj)$ChurnRate, 1)
  
  round(data.frame(Expiry_diff = expiry_diff,
                   Max_diff = max_diff,
                   Target_diff = target_diff,
                   Max_target_diff = max_target_diff,
                   Churn = churn), 3)
}
